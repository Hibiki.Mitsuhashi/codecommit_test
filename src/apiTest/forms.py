from django import forms
from . import models


class PostNumberForm(forms.ModelForm):
    class Meta:
        model = models.PostNumber
        fields = '__all__'